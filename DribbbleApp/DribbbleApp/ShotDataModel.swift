//
//  ShotDataModel.swift
//  DribbbleApp
//
//  Created by James Hoffman on 9/14/15.
//  Copyright (c) 2015 JamesHoffman. All rights reserved.
//

import Alamofire
import SwiftyJSON
import SDWebImage

enum ShotFields: String {
    case Title = "title"
    case Description = "description"
    case Images = "images"
    case ShotImageUrlString = "normal" // TODO: Optimize which shotImageUrlString is chosen depending on screen size
    case ViewCount = "views_count"
//    case PlayerInfo = "player"
    case UserInfo = "user"
    case UserName = "username"
    case MiniAvatarImageUrlString = "avatar_url"
}

class ShotDataModel {
    // Declaring these constants as optionals to allow their values to be set later
    // shotImage and miniUserImage are declared as vars to allow their values to be changed after initial declaration
    let title : String?
    let description : String?
    let userName : String?
    let imageViewCount: Int?
    let shotImageUrlString: String?
    let miniAvatarImageUrlString: String?
//    var shotImage : UIImage? = UIImage(named: "default.png")
//    var miniAvatarImage : UIImage? = UIImage(named: "default.png")
    
    // Initializer takes in a shotJSON
    init(shotJson : JSON) {
        title = shotJson[ShotFields.Title.rawValue].stringValue
        description = shotJson[ShotFields.Description.rawValue].stringValue
        userName = shotJson[ShotFields.UserInfo.rawValue][ShotFields.UserName.rawValue].stringValue
        imageViewCount = shotJson[ShotFields.ViewCount.rawValue].int
        shotImageUrlString = shotJson[ShotFields.Images.rawValue][ShotFields.ShotImageUrlString.rawValue].stringValue
        miniAvatarImageUrlString = shotJson[ShotFields.UserInfo.rawValue][ShotFields.MiniAvatarImageUrlString.rawValue].stringValue
    }
}
