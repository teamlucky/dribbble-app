//
//  ShotCell.swift
//  DribbbleApp
//
//  Created by James Hoffman on 9/15/15.
//  Copyright (c) 2015 JamesHoffman. All rights reserved.
//

import UIKit
import Social

class ShotsCell : UITableViewCell {
    
    @IBOutlet weak var shotImageView: UIImageView!
    @IBOutlet weak var shotTitleLabel: UILabel!
    @IBOutlet weak var imageViewCountLabel: UILabel!
    @IBOutlet weak var twitterButton: subclassedUIButton!
    @IBOutlet weak var facebookButton: subclassedUIButton!

}