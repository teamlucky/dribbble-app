//
//  ShotsWrapperDataModel.swift
//  DribbbleApp
//
//  Created by James Hoffman on 9/14/15.
//  Copyright (c) 2015 JamesHoffman. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

enum ShotsWrapperFields: String {
    case Page = "page"
    case PerPage = "per_page"
    case Pages = "pages"
    case Total = "total"
    case Shots = "shots"
}

class ShotsWrapperDataModel {
    // Declaring these constants as optionals to allow their values to be set later
    var page : Int?
//    var perPage : Int?
//    var pages : Int?
//    var total : Int?
    var shots : Array<ShotDataModel>?
    
    class func JsonToShotsWrapperDataModel(json: JSON?, completion: (ShotsWrapperDataModel?, NSError?) -> Void) {
        // ensure json is not nil and no error is present
        guard json != nil && json!.error == nil else {
            completion (nil, json!.error)
            return
        }

        let unwrappedJson = json!
        
        // Populate shotsWrapperDataModel
        let shotsWrapperDataModel : ShotsWrapperDataModel = ShotsWrapperDataModel()
//        shotsWrapperDataModel.page = unwrappedJson[ShotsWrapperFields.Page.rawValue].intValue
//        shotsWrapperDataModel.perPage = unwrappedJson[ShotsWrapperFields.PerPage.rawValue].intValue
//        shotsWrapperDataModel.pages = unwrappedJson[ShotsWrapperFields.Pages.rawValue].intValue
//        shotsWrapperDataModel.total = unwrappedJson[ShotsWrapperFields.Total.rawValue].intValue

        // Put shotDataModels in an array held by shotsWrapperDataModel
        let shotsJson = unwrappedJson//[ShotsWrapperFields.Shots.rawValue]
        var shotDataModelArray:Array = Array<ShotDataModel>()
        
        for shotJson in shotsJson
        {
            // Do very fast but simple check on validity of URL, does it contain "http"
            // .0 is a SwiftyJSON string, .1 is SwiftyJSON JSON
            if shotJson.1[ShotFields.Images.rawValue][ShotFields.ShotImageUrlString.rawValue].stringValue.lowercaseString.rangeOfString("http") != nil {
                let shotDataModel = ShotDataModel(shotJson: shotJson.1)
                shotDataModelArray.append(shotDataModel)
            }
        }
        shotsWrapperDataModel.shots = shotDataModelArray
        completion (shotsWrapperDataModel, nil)
    }
}



