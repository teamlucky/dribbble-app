//
//  NetworkOperation.swift
//  DribbbleApp
//
//  Created by James Hoffman on 10/6/15.
//  Copyright © 2015 JamesHoffman. All rights reserved.
//

import Alamofire
import SwiftyJSON
import Foundation

let shotsBaseURLString: String = "http://api.dribbble.com/v1/shots?access_token=c1ae7723cfaf54e7ce84135afb67bc9e610c98a7b8a7bdddcc48be4937f0844e&page="

//let shotsBaseURLString: String = "http://api.dribbble.com/shots/popular?page="

class NetworkOperation {

    // Gets first page of shots from API
    // getFirstShots has a completion because it has asynchronous sub-functions
    class func getFirstShotsJson(completion: (Result<JSON>) -> Void) {
        //    class func getFirstShotsJson(completion: (Result<JSON, NSError>) -> Void) { // Alamofire 3.0
        getShotsJsonAtPath(shotsBaseURLString, completion: completion)
    }
    
    // Gets next page of shots from API
    // getMoreShots has a completion because it has asynchronous sub-functions
    class func getMoreShotsJson(shotsWrapperDataModel: ShotsWrapperDataModel?, completion: (Result<JSON>) -> Void) {
        //    class func getMoreShotsJson(shotsWrapperDataModel: ShotsWrapperDataModel?, completion: (Result<JSON, NSError>) -> Void) { // Alamofire 3.0
        if let currentPage = shotsWrapperDataModel?.page {
            let nextPageUrlString = shotsBaseURLString + "\(currentPage)"
            getShotsJsonAtPath(nextPageUrlString, completion: completion)
        }
    }
    
    // Utilizes Alamofire extension to return specified ShotsWrapperDataModel from API
    // getShotsAtPath has a completion because it has asynchronous sub-functions
    private class func getShotsJsonAtPath(path: String, completion: (Result<JSON>) -> Void) {
        //    private class func getShotsJsonAtPath(path: String, completion: (Result<JSON, NSError>) -> Void) {  // Alamofire 3.0
        Alamofire.request(.GET, path).responseSwiftyJSON {
            (_, _, result) in
            //            response in // Alamofire 3.0
            
            // Print debugDescription if responseSwiftyJSON errored
            if result.isFailure == true {
                print(result.debugDescription)
            }
            
            completion(result)
            //          completion(response) // Alamofire 3.0
        }
    }

}