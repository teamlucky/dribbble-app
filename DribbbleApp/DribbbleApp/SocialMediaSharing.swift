//
//  SocialMediaSharing.swift
//  DribbbleApp
//
//  Created by James Hoffman on 10/8/15.
//  Copyright © 2015 JamesHoffman. All rights reserved.
//

import Foundation
import UIKit
import Social

enum SocialMediaEnum : String {
    case Twitter = "Twitter"
    case Facebook = "Facebook"
    
    // This internal enum function returns the SLServiceType associated with the current enum value
    // SLServiceTypeTwitter and SLServiceTypeFacebook are keywords with pre-defined String values
    func associatedService() -> String {
        var serviceName: String
        
        switch self {
        case .Twitter:
            serviceName = SLServiceTypeTwitter
        case .Facebook:
            serviceName = SLServiceTypeFacebook
        }
        return serviceName
    }
}

class socialMediaSharing : SocialMediaSharingDelegate {
    
    func shareOn(let socialMediaService: SocialMediaEnum, sender: subclassedUIButton?, currentViewController: UIViewController) {
        // Ensure social media service is available
        guard SLComposeViewController.isAvailableForServiceType(socialMediaService.associatedService())  else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a \(socialMediaService.rawValue) account to continue.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            currentViewController.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        let serviceViewController :SLComposeViewController = SLComposeViewController(forServiceType: socialMediaService.associatedService())
        
        // Add image URL from image in same cell as sending button
        // The URL for each image was store in its associated button at instantiation
        if let imageUrl = sender?.urlString {
            if let url:NSURL = NSURL(string: imageUrl  as String) {
                serviceViewController.addURL(url)
            }
        }
        currentViewController.presentViewController(serviceViewController, animated: true, completion: nil)
    }
}