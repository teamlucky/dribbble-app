//
//  DetailViewController.swift
//  DribbbleApp
//
//  Created by James Hoffman on 9/13/15.
//  Copyright (c) 2015 JamesHoffman. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {

    // View Objects (from top to bottom, left to right, and bottom layer to top layer)
    @IBOutlet var detailUIView: UIView!
    @IBOutlet weak var detailShotUIView: UIView!
    @IBOutlet weak var shotImageView: UIImageView!
    @IBOutlet weak var shotTitleLabel: UILabel!
    @IBOutlet weak var imageViewCountLabel: UILabel!
    @IBOutlet weak var miniAvatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var shotDescriptionText: UITextView!
    
    // Model Objects
    var shotDataModel: ShotDataModel? {  // Value set in MasterViewController segueToShotDetailView
        didSet {
            // Update the view.
            self.setDetailViewAttributes()
            self.configureView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }

    func configureView() {
        // Formatting miniUserImage: Rounded
        miniAvatarImageView?.layer.cornerRadius = 17.8
        miniAvatarImageView?.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setDetailViewAttributes() {
        // Update UI in Main Queue using ShotDataModel
        dispatch_async(dispatch_get_main_queue()){

            if let shotTitleLabel = self.shotDataModel?.title {
                self.shotTitleLabel?.text = shotTitleLabel
            }
            if let imageViewCountLabel = self.shotDataModel?.imageViewCount {
                self.imageViewCountLabel?.text = "\(imageViewCountLabel)"
            }
            if let userName = self.shotDataModel?.userName {
                self.userNameLabel?.text = userName
            }
            if let description = self.shotDataModel?.description {
                // Interpret HTML text
                if let attributedShotDescription = try? NSAttributedString(
                    data: description.dataUsingEncoding(NSUnicodeStringEncoding, allowLossyConversion: true)!,
                    options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                    documentAttributes: nil) {
                        
                    self.shotDescriptionText?.attributedText = attributedShotDescription
                }
            }
            
            // Set shotImageView Image from Cache
            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            }
            if let url:NSURL = NSURL(string: self.shotDataModel!.shotImageUrlString!  as String) {
                self.shotImageView?.sd_setImageWithURL(url, placeholderImage:UIImage(named: "default.png"),completed:block)
            }
            
            // Set miniAvatarImageView
            let block2: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            }
            if let url:NSURL = NSURL(string: self.shotDataModel!.miniAvatarImageUrlString!  as String) {
                self.miniAvatarImageView?.sd_setImageWithURL(url, placeholderImage:UIImage(named: "default.png"),completed:block2)
            }
        }
    }
}

