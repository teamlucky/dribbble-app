//
//  AlamofireExtension.swift
//  DribbbleApp
//
//  Created by James Hoffman on 9/14/15.
//  Copyright (c) 2015 JamesHoffman. All rights reserved.
//

import Alamofire
import SwiftyJSON

extension Alamofire.Request {
    
    func responseSwiftyJSON(
            completion: (NSURLRequest?, NSHTTPURLResponse?, Result<JSON>) -> Void
//            completion: Result<JSON, NSError> -> Void // Alamofire 3.0
         ) -> Self
    {
        
        // Custom responseSerializer
        let responseSerializer = GenericResponseSerializer<JSON> { request, response, data in
//        let responseSerializer = ResponseSerializer<JSON, NSError> { _, _, data, error in  // Alamofire 3.0
//            guard error == nil else { return .Failure(error!) } // new in Alamofire 3.0
            
            // Ensure there is a successful HTTP response
            guard let httpResponse = response else {
                print("NSHTTPURLResponse optional is nil.")
                let error = Error.errorWithCode(.StatusCodeValidationFailed, failureReason: "HTTP Response missing.")
                // Return GenericResponseSerializer<JSON> failure
                return Result.Failure(data, error)
//                return Result.Failure(error) // Alamofire 3.0
            }
            guard httpResponse.statusCode == 200 else {
                print("HTTP Response was unsuccessful. Status code \(httpResponse.statusCode). Request: \(request)")
                let error = Error.errorWithCode(.StatusCodeValidationFailed, failureReason: "HTTP Response was unsuccessful. Status code \(httpResponse.statusCode).")
                // Return GenericResponseSerializer<JSON> failure
                return Result.Failure(data, error)
//                return Result.Failure(error) // Alamofire 3.0
            }
            
            // Ensure data is valid and unwrap it
            guard let validData = data where validData.length > 0 else {
                print("JSON could not be serialized because input data was nil or zero length.")
                let error = Error.errorWithCode(.JSONSerializationFailed, failureReason: "Invalid data")
                // Return GenericResponseSerializer<JSON> failure
                return Result.Failure(data, error)
//                return Result.Failure(error) // Alamofire 3.0
            }
            
            // Creating JSON Object from serialized data
            let jsonData: AnyObject?
            // Use do-try-catch syntax because JSONObjectWithData might throw an error (has "throws" in function definition)
            do {
                jsonData = try NSJSONSerialization.JSONObjectWithData(validData, options: [])
            } catch {
                // Local variable "error" automatically generated for catch without pattern
                // Return GenericResponseSerializer<JSON> failure
                return Result.Failure(data, error as NSError)
//                return Result.Failure(error as NSError) // Alamofire 3.0
            }
            
            // Using SwiftyJSON to put JSON in a more convenient format
            let json = SwiftyJSON.JSON(jsonData!)
        
            // Return GenericResponseSerializer<JSON> success
            return Result.Success(json)
        }
        
        // Alamofire's generic response handler
        // Returns to responseSwiftyJSON
        return self.response(
                responseSerializer: responseSerializer,
                completionHandler: completion
        )
    }
    
}
