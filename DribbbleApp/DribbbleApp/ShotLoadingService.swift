//
//  ShotLoadingService.swift
//  DribbbleApp
//
//  Created by James Hoffman on 10/8/15.
//  Copyright © 2015 JamesHoffman. All rights reserved.
//

import UIKit
import Foundation

class ShotLoadingService : ShotLoadingServiceDelegate {

    var isLoadingShots = false
    var currentShotsWrapper:ShotsWrapperDataModel?
    
    // Loads first page of shots from API to shotsDataModelArray
    func loadFirstShots(currentViewController: UIViewController, shotTableView: UITableView!, inout shotsDataModelArray:Array<ShotDataModel>?)
    {
        isLoadingShots = true
        NetworkOperation.getFirstShotsJson({ (firstShotsJsonResult) in
            // Ensure firstMoreShotsJson was a success
            guard firstShotsJsonResult.isSuccess else {
                self.errorAlerting("Could not load first shots \(firstShotsJsonResult.error)", currentViewController: currentViewController)
                return
            }
            
            ShotsWrapperDataModel.JsonToShotsWrapperDataModel(firstShotsJsonResult.value){ (shotsWrapperDataModel, error) in
                // Ensure data conversion to shotsWrapperDataModel was successful
                guard error == nil else {
                    self.errorAlerting("Could not load first shots \(error?.localizedDescription)", currentViewController: currentViewController)
                    return
                }
                //                guard shotsWrapperDataModel?.total != 0 &&
                guard shotsWrapperDataModel?.shots != nil else {
                    self.errorAlerting("Could not load first shots. Incorrect API format.", currentViewController: currentViewController)
                    return
                }
                
                self.addShotsFromWrapper(shotsWrapperDataModel, shotsDataModelArray: &shotsDataModelArray)
                self.isLoadingShots = false
                shotTableView?.reloadData()
            }
            
        })
    }
    
    // Loads another page of shots from API to shotsDataModelArray
    func loadMoreShots(currentViewController: UIViewController)
    {
//        self.isLoadingShots = true
//        
//        guard currentViewController.shotsDataModelArray != nil &&
//            self.currentShotsWrapper != nil
//            //              self.shotsDataModelArray!.count < self.currentShotsWrapper?.total &&
//            //              self.currentShotsWrapper?.page != self.currentShotsWrapper?.pages
//            else {
//                return
//        }
//        
//        self.currentShotsWrapper?.page = self.shotsPage
//        
//        NetworkOperation.getMoreShotsJson(self.currentShotsWrapper, completion: { (moreShotsJsonResult) in
//            // Ensure getMoreShotsJson was a success
//            guard moreShotsJsonResult.isSuccess else {
//                self.errorAlerting("Could not load more shots \(moreShotsJsonResult.error)")
//                return
//            }
//            
//            ShotsWrapperDataModel.JsonToShotsWrapperDataModel(moreShotsJsonResult.value){ (shotsWrapperDataModel, error) in
//                // Ensure data conversion to shotsWrapperDataModel was successful
//                guard error == nil else {
//                    self.errorAlerting("Could not load first shots \(error?.localizedDescription)")
//                    return
//                }
//                self.addShotsFromWrapper(shotsWrapperDataModel)
//                self.isLoadingShots = false
//                self.shotTableView?.reloadData()
//                self.shotsPage += 1
//            }
//        })
        
    }
    
    // Put shotsWrapperDataModel shots in shotsDataModelArray
    private func addShotsFromWrapper(shotsWrapperDataModel: ShotsWrapperDataModel?, inout shotsDataModelArray:Array<ShotDataModel>?)
    {
        self.currentShotsWrapper = shotsWrapperDataModel
        
        if shotsDataModelArray == nil
        {
            shotsDataModelArray = self.currentShotsWrapper?.shots
            print("got here")
        }
        else if self.currentShotsWrapper != nil && self.currentShotsWrapper!.shots != nil
        {
            shotsDataModelArray = shotsDataModelArray! + self.currentShotsWrapper!.shots!
        }
    }
    
    func errorAlerting(message: String, currentViewController: UIViewController){
        self.isLoadingShots = false
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
        currentViewController.presentViewController(alert, animated: true, completion: nil)
    }

    
}