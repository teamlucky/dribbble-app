//
//  MasterViewController.swift
//  DribbbleApp
//
//  Created by James Hoffman on 9/13/15.
//  Copyright (c) 2015 JamesHoffman. All rights reserved.
//

import UIKit
import SDWebImage

protocol SocialMediaSharingDelegate {
    func shareOn(socialMediaService: SocialMediaEnum, sender: subclassedUIButton?, currentViewController: UIViewController)
}

class MasterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // Delegates
    let socialMediaSharingDelegate : SocialMediaSharingDelegate = socialMediaSharing()

    // View Objects (from top to bottom, left to right, and bottom layer to top layer)
    @IBOutlet var masterUIView: UIView!
    @IBOutlet weak var shotTableView: UITableView!
    

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        // Call refreshView when refreshControl is activated
        refreshControl.addTarget(self, action: "refreshView:", forControlEvents: UIControlEvents.ValueChanged)
        return refreshControl
        }()
    
    //Locals
    var shotsDataModelArray: Array<ShotDataModel>?
    var currentShotsWrapper: ShotsWrapperDataModel?
    var isLoadingShots = false
    var shotsPage : Int = 2  // The first page is obtained by getFirstShotsJson, therefore the counter for getMoreShotsJson must begin on page 2
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        self.loadFirstShots()
        
        // Add refresh control to table view
        self.shotTableView?.addSubview(self.refreshControl)
    }
    
    func configureView() {
        // Adjust shotsCell height based on UIView and navigation controller heights
        let statusBarHeight: CGFloat = UIApplication.sharedApplication().statusBarFrame.size.height
        if let navHeight = self.navigationController?.navigationBar.frame.height,
               masterUIViewFrameHeight = masterUIView?.frame.height {
            shotTableView?.rowHeight = (masterUIViewFrameHeight - statusBarHeight - 2 * navHeight) / 2
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.shotTableView?.reloadData()
    }
    
    // Executed when refresh control is activated
    func refreshView(refreshControl: UIRefreshControl) {
        self.loadFirstShots()
        self.shotTableView?.reloadData()
        refreshControl.endRefreshing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.shotsDataModelArray == nil
        {
            return 0
        }
        return self.shotsDataModelArray!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("shotsCell") as! ShotsCell
        
        if let shotDataModel = self.shotsDataModelArray?[indexPath.row] {
            
            if let shotTitleLabel = shotDataModel.title {
                cell.shotTitleLabel?.text = shotTitleLabel
            }
            if let imageViewCountLabel = shotDataModel.imageViewCount {
                cell.imageViewCountLabel?.text = "\(imageViewCountLabel)"
            }
            
            // Set shotImageView Image from URL
            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            }
            if let url:NSURL = NSURL(string: shotDataModel.shotImageUrlString!  as String) {
                cell.shotImageView?.sd_setImageWithURL(url, placeholderImage:UIImage(named: "default.png"),completed:block)
            }
            
//            cell.imageViewCountView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
            
            
            
            cell.twitterButton.urlString = shotDataModel.shotImageUrlString!
            cell.facebookButton.urlString = shotDataModel.shotImageUrlString!
            configureSocialMediaButtons(cell)
        }
        
         if self.shotsDataModelArray != nil && self.shotsDataModelArray!.count >= indexPath.row
         {
            // See if we need to load more shots
            let rowsToLoadFromBottom = 5;
            let rowsLoaded = self.shotsDataModelArray!.count
            if (!self.isLoadingShots && (indexPath.row >= (rowsLoaded - rowsToLoadFromBottom)))
            {
//                if let totalRows = self.currentShotsWrapper?.total {
//                    let remainingShotsToLoad = totalRows - rowsLoaded;

//                    if (remainingShotsToLoad > 0)
//                    {
                        self.loadMoreShots()
//                    }
//                }
            }
          }
    
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        var row = indexPath.row
//        shotTableView.cellForRowAtIndexPath(indexPath)?.contentView
    
        performSegueWithIdentifier("segueToShotDetailView", sender: self)
    }

    func configureSocialMediaButtons(cell: ShotsCell) {
        // Configure share buttons in cell: Light Background, Rounded
        cell.twitterButton?.backgroundColor = UIColor(white: 0, alpha: 0.15)
        cell.twitterButton?.layer.cornerRadius = 14
        cell.twitterButton?.clipsToBounds = true
        cell.twitterButton?.addTarget(self, action: "twitterShare:", forControlEvents: .TouchUpInside)
        cell.facebookButton?.backgroundColor = UIColor(white: 0, alpha: 0.15)
        cell.facebookButton?.layer.cornerRadius = 14
        cell.facebookButton?.clipsToBounds = true
        cell.facebookButton?.addTarget(self, action: "facebookShare:", forControlEvents: .TouchUpInside)
    }
    
    // MARK: - Delegate Methods
    
    func twitterShare(sender: subclassedUIButton?) {
//        socialMediaSharingDelegate.twitterShare(sender, currentViewController: self)
        socialMediaSharingDelegate.shareOn(SocialMediaEnum.Twitter, sender: sender, currentViewController: self)
    }

    func facebookShare(sender: subclassedUIButton?) {
//        socialMediaSharingDelegate.facebookShare(sender, currentViewController: self)
        socialMediaSharingDelegate.shareOn(SocialMediaEnum.Facebook, sender: sender, currentViewController: self)
    }
    
    // MARK: - Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueToShotDetailView" {
            if let indexPath = self.shotTableView?.indexPathForSelectedRow {
                let shotDataModel = shotsDataModelArray?[indexPath.row]
                let referenceToDetailView = (segue.destinationViewController as! DetailViewController)
                referenceToDetailView.shotDataModel = shotDataModel
            }
        }
    }
    
    // MARK: - Loading Shots
    // TODO: Create a ShotsService with these functions so they don't reside in View Controller
    
    // Loads first page of shots from API to shotsDataModelArray
    func loadFirstShots()
    {
        isLoadingShots = true
        NetworkOperation.getFirstShotsJson({ (firstShotsJsonResult) in
            // Ensure firstMoreShotsJson was a success
            guard firstShotsJsonResult.isSuccess else {
                self.errorAlerting("Could not load first shots \(firstShotsJsonResult.error)")
                return
            }
            
            ShotsWrapperDataModel.JsonToShotsWrapperDataModel(firstShotsJsonResult.value){ (shotsWrapperDataModel, error) in
                // Ensure data conversion to shotsWrapperDataModel was successful
                guard error == nil else {
                    self.errorAlerting("Could not load first shots \(error?.localizedDescription)")
                    return
                }
//                guard shotsWrapperDataModel?.total != 0 &&
                guard shotsWrapperDataModel?.shots != nil else {
                    self.errorAlerting("Could not load first shots. Incorrect API format.")
                    return
                }
                
                self.addShotsFromWrapper(shotsWrapperDataModel)
                self.isLoadingShots = false
                self.shotTableView?.reloadData()
            }
            
        })
    }
    
    // Loads another page of shots from API to shotsDataModelArray
    func loadMoreShots()
    {
        self.isLoadingShots = true
        
        guard self.shotsDataModelArray != nil &&
              self.currentShotsWrapper != nil
//              self.shotsDataModelArray!.count < self.currentShotsWrapper?.total &&
//              self.currentShotsWrapper?.page != self.currentShotsWrapper?.pages 
        else {
            return
        }
        
        self.currentShotsWrapper?.page = self.shotsPage
        
        NetworkOperation.getMoreShotsJson(self.currentShotsWrapper, completion: { (moreShotsJsonResult) in
            // Ensure getMoreShotsJson was a success
            guard moreShotsJsonResult.isSuccess else {
                self.errorAlerting("Could not load more shots \(moreShotsJsonResult.error)")
                return
            }
            
            ShotsWrapperDataModel.JsonToShotsWrapperDataModel(moreShotsJsonResult.value){ (shotsWrapperDataModel, error) in
                // Ensure data conversion to shotsWrapperDataModel was successful
                guard error == nil else {
                    self.errorAlerting("Could not load first shots \(error?.localizedDescription)")
                    return
                }
                self.addShotsFromWrapper(shotsWrapperDataModel)
                self.isLoadingShots = false
                self.shotTableView?.reloadData()
                self.shotsPage += 1
            }
        })
        
    }
    
    // Put shotsWrapperDataModel shots in shotsDataModelArray
    func addShotsFromWrapper(shotsWrapperDataModel: ShotsWrapperDataModel?)
    {
        self.currentShotsWrapper = shotsWrapperDataModel

        if self.shotsDataModelArray == nil
        {
            self.shotsDataModelArray = self.currentShotsWrapper?.shots
        }
        else if self.currentShotsWrapper != nil && self.currentShotsWrapper!.shots != nil
        {
            self.shotsDataModelArray = self.shotsDataModelArray! + self.currentShotsWrapper!.shots!
        }
    }
    
    func errorAlerting(message: String){
        self.isLoadingShots = false
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

